import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue'
import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import LoggedIn from './components/LoggedIn.vue'

const routes = [
  {
    path: '/',
    name: 'App',
    component: App
  },
  {
    path: '/user/logIn',
    name: "logIn",
    component: LogIn
  },

  {
    path: '/user/signUp',
    name: 'signUp',
    component: SignUp
  },
  {
    path: '/user/loggedIn',
    name: 'loggedIn',
    component: LoggedIn
  } 
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
